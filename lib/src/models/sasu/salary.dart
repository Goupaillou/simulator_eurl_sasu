part of simulator_eurl_sasu;

class Salary {

  Logger logger = new Logger('simulator.SalarySas');

  double chargesEmployee;
  double chargesEmployer;

  double extraGross;
  double gross;
  double net;
  double netTaxable;

  List<ChargeSasComputed> chargesComputed = new List();

  convertAnnualToMonthly() {
    chargesEmployee = chargesEmployee/12;
    chargesEmployer = chargesEmployer/12;
    extraGross = extraGross/12;
    gross = gross/12;
    net = net/12;
    netTaxable = netTaxable/12;

    chargesComputed.forEach((charge) {
      charge.convertAnnualToMonthly();
    });

    return this;
  }

  Salary.computeFromCharges(double gross, List<ChargeSasComputed> chargesComputed, List<ChargeSasCsgCrds> chargesCsgCrds) {
    this.gross = gross;
    this.chargesComputed = chargesComputed;

    List<ChargeSasComputed> chargesComputedToAddToCsgCrdsBase = chargesComputed.where((ChargeSasComputed chargeSasComputed)  => chargeSasComputed.charge.toAddToCsgCrdsBase).toList();
    double sumChargesToAddToCsgCrdsBase = chargesComputedToAddToCsgCrdsBase.map((charge) => charge.partEmployerComputed).reduce((charge1, charge2) => charge1 + charge2);

    for(ChargeSasCsgCrds charge in chargesCsgCrds) {
      double newBaseCalcul = charge.calculateBase(this.gross, sumChargesToAddToCsgCrdsBase);
      this.chargesComputed.add(new ChargeSasComputed(charge, charge.computeEmployee(newBaseCalcul), charge.computeEmployer(newBaseCalcul)));
    }

    chargesEmployee = 0.00;
    chargesEmployer = 0.00;
    for(ChargeSasComputed chargeComputed in this.chargesComputed) {
      chargesEmployee += chargeComputed.partEmployeeComputed;
      chargesEmployer += chargeComputed.partEmployerComputed;
    }

    net = this.gross - chargesEmployee;

    List<ChargeSasComputed> chargesComputedNonDeductibles = chargesComputed.where((ChargeSasComputed chargeSasComputed) {
      if(chargeSasComputed.charge is ChargeSasCsgCrds) {
        ChargeSasCsgCrds charge = chargeSasComputed.charge;
        if(charge.isDeductible == false) {
          return true;
        }
      }
      return false;
    }).toList();

    double sumChargesIndeductible = chargesComputedNonDeductibles.map((charge) => charge.partEmployeeComputed).reduce((charge1, charge2) => charge1 + charge2);
    netTaxable = net + sumChargesIndeductible;

    extraGross = this.gross + chargesEmployer;
  }

  Map toPublicJson() {
    Map map = new Map();
    map["gross"] = roundNumber(gross);
    map["net"] = roundNumber(net);
    map["extraGross"] = roundNumber(extraGross);
    map["netTaxable"] = roundNumber(netTaxable);

    List charges = new List();
    for(ChargeSasComputed charge in chargesComputed) {
      charges.add(charge.toPublicJson());
    }

    map["charges"] = charges;

    return map;
  }

  String toString() {
    return """
      \n
      [Salary]
      gross = $gross
      net = $net
      netTaxable = $netTaxable
      extraGross = $extraGross
      chargesEmployee = $chargesEmployee
      chargesEmployer = $chargesEmployer            
      """;
  }
}
