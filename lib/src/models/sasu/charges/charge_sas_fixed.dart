part of simulator_eurl_sasu;



class ChargeSasFixed extends ChargeSas {

  double amountEmployee;
  double amountEmployer;

  ChargeSasFixed(String name, this.amountEmployee, this.amountEmployer, {toAddToCsgCrdsBase: false})
      : super(name, 0.0, 0.0, toAddToCsgCrdsBase: toAddToCsgCrdsBase);

  @override
  double computeEmployer(double amount) {
    return amountEmployer;
  }

  @override
  double computeEmployee(double amount) {
    return amountEmployee;
  }
}
