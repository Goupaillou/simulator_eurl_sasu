part of simulator_eurl_sasu;



class ChargeSasGmp extends ChargeSas {

  double gmpThreshold = 43337.76;
  double gmpYearEmployee = 320.52;
  double gmpYearEmployer = 524.04;

  ChargeSasGmp(String name, double rateEmployee, double rateEmployer)
      : super(name, rateEmployee, rateEmployer);

  @override
  double computeEmployer(double amount) {
    if (amount >= gmpThreshold || amount == 0.00) {
      return 0.00;
    }

    if (amount <= pass) {
      return gmpYearEmployer;
    }

    double amountToProcess = amount - pass;
    double partComputed = amountToProcess * rateEmployer / 100;
    return gmpYearEmployer - partComputed;
  }

  @override
  double computeEmployee(double amount) {
    if (amount >= gmpThreshold || amount == 0.00) {
      return 0.00;
    }

    if (amount <= pass) {
      return gmpYearEmployee;
    }

    double amountToProcess = amount - pass;
    double partComputed = amountToProcess * rateEmployee / 100;
    return gmpYearEmployee - partComputed;
  }
}
