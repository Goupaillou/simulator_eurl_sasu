part of simulator_eurl_sasu;


class ChargeSasLinear extends ChargeSas {

  double chargeNullIfExceeds = null;
  double chargeNullIfBelow = null;

  ChargeSasLinear(String name, double rateEmployee, double rateEmployer, {toAddToCsgCrdsBase: false, this.chargeNullIfExceeds, this.chargeNullIfBelow}) : super(name, rateEmployee, rateEmployer, toAddToCsgCrdsBase: toAddToCsgCrdsBase);

  @override
  double computeEmployer(double amount) {
    if(chargeNullIfExceeds != null && amount > chargeNullIfExceeds) {
      return 0;
    }
    if(chargeNullIfBelow != null && amount <= chargeNullIfBelow) {
      return 0;
    }

    return amount * rateEmployer / 100;
  }

  @override
  double computeEmployee(double amount) {
    if(chargeNullIfExceeds != null && amount > chargeNullIfExceeds) {
      return 0;
    }
    if(chargeNullIfBelow != null && amount <= chargeNullIfBelow) {
      return 0;
    }

    return amount * rateEmployee / 100;
  }
}