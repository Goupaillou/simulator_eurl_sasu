part of simulator_eurl_sasu;


abstract class ChargeSas implements ComputeSas {

  Logger logger = new Logger('simulator.ChargeEurl');

  String name;
  double rateEmployee;
  double rateEmployer;

  bool toAddToCsgCrdsBase = false;
  bool toApplyToEmployeeOnly = false;

  ChargeSas(this.name, this.rateEmployee, this.rateEmployer, {this.toAddToCsgCrdsBase : false, this.toApplyToEmployeeOnly : false});

  String toString() {
    return """
      \n
      [ChargeSas]
      name = $name
      rateEmployee = $rateEmployee
      rateEmployer = $rateEmployer
      """;
  }
}

abstract class ComputeSas {
  double computeEmployee(double amount);
  double computeEmployer(double amount);
}

class ChargeSasComputed {

  Logger logger = new Logger('simulator.ChargeSasComputed');

  ChargeSas charge;
  double partEmployeeComputed;
  double partEmployerComputed;

  ChargeSasComputed(this.charge, this.partEmployeeComputed, this.partEmployerComputed);

  convertAnnualToMonthly() {
    partEmployeeComputed = partEmployeeComputed/12;
    partEmployerComputed = partEmployerComputed/12;
  }

  Map toPublicJson() {
    Map map = new Map();
    map["name"] = charge.name;
    map["partEmployee"] = roundNumber(partEmployeeComputed);
    map["partEmployer"] = roundNumber(partEmployerComputed);

    return map;
  }

  String toString() {
    return """
      \n
      [ChargeSasComputed]
      charge = $charge
      partEmployeeComputed = $partEmployeeComputed
      partEmployerComputed = $partEmployerComputed
      """;
  }
}
