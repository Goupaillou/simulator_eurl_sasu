part of simulator_eurl_sasu;


ServiceDividend serviceDividend = new ServiceDividend();

class ServiceDividend {

  Logger logger = new Logger('simulator.ServiceDividend');

  Dividend computeDividend(double dividendGross) {

    double flatTax = 30;
    double charges = dividendGross * (flatTax / 100);
    double net = dividendGross - charges;

    return new Dividend(dividendGross, charges, net);
  }
}

